#!/bin/perl -w

# $Id: bmprint_1yr,v 1.1 2003/08/12 17:27:46 bmy Exp $

=head1 NAME

bmprint - produces a GEOS-CHEM benchmark simulation

=head1 SYNOPSIS

bmprint -v VERSION -Rn

where:
   -v VERSION specifies the GEOS-CHEM version number (e.g. 5.01)
   -Rn        specifies that a Radon simulation will take place
               instead of a full chemistry simulation  

=head1 REQUIRES

Perl 5.003
BenchMark.pm

=head1 EXPORTS

None

=head1 DESCRIPTION

bmprint
Methods are provided for printing GEOS-CHEM benchmark simulation output

=head1 METHODS
&getArguments     : Reads arguments from the command line
&copyInputFiles   : Copies input files to the run directory
&runTheModel      : Creates job script and submits it to queue system
&main             : Driver program for this script

=head1 MODIFICATION HISTORY

bmy, 20 Aug 2002: Initial version
bmy, 16 Sep 2002: Do not print the emissions file when -Rn is selected 
bmy, 03 Jan 2003: Now use qx() instead of backticks

=head1 AUTHOR

Bob Yantosca (bmy@io.harvard.edu)

=head1 SEE ALSO

BenchMark.pm

=head1 COPYRIGHT

Copyright 2002, Bob Yantosca.  All rights reserved.

=cut

#------------------------------------------------------------------------------

require 5.003;      # need this version of Perl or newer
use English;        # Use English language
use Carp;           # Get detailed error messages
use strict;         # Force explicit variable declarations (like IMPLICIT NONE)

# Import routines from BenchMark.pm
use BenchMark qw( &printSeparator &getVersionString 
                  &getCodeDir     &getOutputDir );  

#------------------------------------------------------------------------------

sub getArguments() {

  #===========================================================================
  # Subroutine getArguments gets arguments from the command line. 
  # (bmy, 8/20/02, 1/3/03)
  #
  # Arguments as Output:
  # --------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  # (2 ) $simType : Simulation type (Radon or full chemistry)
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # ( $vNum, $simType ) = &getArguments();
  #
  # NOTES:
  #===========================================================================

  # Local variables
  my $i       = "";
  my $vNum    = "";
  my $simType = "";

  #===========================================================================
  # getArguments begins here! 
  #===========================================================================
  for ( $i = 0; $i < scalar( @ARGV ); $i++ ) {

    # Test for version number
    if ( $ARGV[$i] =~ /^-v/ ) {
      $vNum = $ARGV[++$i];
      
    # Test for Radon simulation flag
    } elsif ( $ARGV[$i] =~ /^-Rn/ ) {
      $simType = "Rn-Pb-Be";

    # Otherwise do nothing 
    } else {}
  }

  # Error Check $vNum
  if ( length( $vNum ) == 0 ) {
    &printSeparator();
    print "The version number must be specified at the command line!\n";
    print "Example: bmprint -v 5.01\n";
    print "STOP in routine 'getArguments' of script 'bmprint'!\n";
    &printSeparator();
    exit(1);
  }
  
  # Use defaults for simulation type
  if ( length( $simType ) == 0 ) { $simType = "fullchem"; }

  # Return to calling program
  return( $vNum, $simType );
}

#------------------------------------------------------------------------------

sub getPrinters() {
  
  #===========================================================================
  # Subroutine getPrinters returns the names of the B/W and COLOR PRINTERS
  # that will be used to print results from the benchmark simulation.
  # (bmy, 8/20/02, 1/3/03)
  #
  # NOTES:
  # (1 ) Printer names are hardwired -- change as necessary (bmy, 8/20/02)
  #===========================================================================

  # Comment out for now (bmy, 5/16/03)
  #my $bwPrinter    = "mv";
  #my $colorPrinter = "phaserP";

  my $bwPrinter    = "phall";
  my $colorPrinter = "maximus";

  # Return to calling program
  return( $bwPrinter, $colorPrinter );
}

#------------------------------------------------------------------------------

sub main() {

  #==========================================================================
  # MAIN is the driver program for "bmprint".  It does the following:
  # (1) Reads arguments from the command line
  # (2) Sends text files to the B/W printer
  # (3) Sends B/W PostScript files to the B/W printer
  # (4) Sends color PostScript files to the color printer
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # bmprint -v VERSION -Rn
  #
  # Where:
  #   -v VERSION specifies the GEOS-CHEM version number (e.g. 5.01)
  #   -Rn        will print files from a Rn-Pb-Be simulation
  #               instead of from a full chemistry simulation
  #
  # NOTES:
  # (1 ) Don't print the emissions file for a Rn benchmark (bmy, 9/16/02)
  # (2 ) Now use qx() instead of backticks (bmy, 1/3/03)
  #==========================================================================

  # Local variables
  my $vNum         = "";
  my $simType      = "";
  my $bwPrinter    = "";
  my $colorPrinter = "";
  my $vString      = "";
  my $outputDir    = "";
  my $codeDir      = "";
  my $prtCmd       = "";

  #==========================================================================
  # main begins here! 
  #==========================================================================

  # Get arguments from the command line
  ( $vNum, $simType ) = &getArguments();

  # Get the B/W and COLOR PRINTER names
  ( $bwPrinter, $colorPrinter ) = &getPrinters();

  # Get the version string
  $vString   = &getVersionString( $vNum );

  # Get the code and output directories
  $codeDir   = &getCodeDir( $vNum, $simType );
  $outputDir = &getOutputDir( $vNum, $simType );

  # Send TEXT FILES to the printer using the LPTOPS command
  $prtCmd = "| lptops -V -P10pt -U -G | lpr -P$bwPrinter";
  qx( cat $codeDir/REVISIONS $prtCmd );
  qx( cat $outputDir/$vString.budget.$simType $prtCmd );    

  # Only print out emissions file for full chemistry run
  if ( $simType =~ "fullchem" ) {
    qx( cat $outputDir/$vString.emissions.$simType $prtCmd );  
  }

  # Send B/W POSTSCRIPT files to a B/W PRINTER
  $prtCmd = "lpr -P$bwPrinter -s";
  qx( $prtCmd $outputDir/$vString.freq_ratio.ps   );
  qx( $prtCmd $outputDir/$vString.tracer_zonal.ps );

  # Send COLOR POSTSCRIPT files to the COLOR PRINTER
  $prtCmd = "lpr -P$colorPrinter -s";
  qx( $prtCmd $outputDir/$vString.tracer_map_surface.ps   );
  qx( $prtCmd $outputDir/$vString.tracer_map_500hPa.ps    );
  qx( $prtCmd $outputDir/$vString.tracer_ratio_surface.ps );
  qx( $prtCmd $outputDir/$vString.tracer_ratio_500hPa.ps  );

  # Only print the J-Value ratio file for a FULL CHEMISTRY simulation
  if ( $simType =~ "fullchem" ) {
    qx( $prtCmd $outputDir/$vString.jv_ratio.ps );
  }

  # Exit Normally
  exit(0);
}

#------------------------------------------------------------------------------

# Call MAIN
main();

# Exit normally
exit(0)
