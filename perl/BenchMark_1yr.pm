#!/bin/perl -w

# $Id: BenchMark_1yr.pm,v 1.5 2005/02/15 14:38:56 bmy Exp $

=head1 NAME

BenchMark_1yr - contains utility methods for GEOS-CHEM 1yr benchmark runs
            
=head1 SYNOPSIS

use BenchMark_1yr

=head1 REQUIRES

Perl 5.003

=head1 EXPORTS

&printSeparator, &getUser,    &getVersionString, &getCodeDir,
&getInputDir,    &getRootDir, &getRunDir,        &getOutputDir,
&getJobFile,     &getLogFile, &getCmpFile,       &dateString

=head1 DESCRIPTION

BenchMark_1yr
Methods are provided for returning directory names, file names,
etc. for a GEOS-CHEM benchmark simulation

=head1 METHODS

&printSeparator   : Prints a separator line to stdout
&getUser          : Returns the user\'s home directory path
&getVersionString : Returns the version string (e.g. "v5-01")
&getCodeDir       : Returns the source code directory name
&getInputDir      : Returns the input file directory name
&getRootDir       : Returns the top-level run directory name
&getRunDir        : Returns the individual run directory name
&getOutputDir     : Returns the output directory name
&getJobFile       : Returns the name of the job script
&getLogFile       : Returns the name of the log file
&getCmpFile       : Returns the name of the compilation file
&getMakeFile      : Returns the proper makefile name
&getModelType     : Returns the model type (GEOS-3, GEOS-4, etc)
&dateString       : Returns a string in DDDHHMMSS format

=head1 MODIFICATION HISTORY

bmy, 20 Aug 2002 - INITIAL VERSION
bmy, 23 Mar 2003 - Now can handle branch versions (e.g. v5-03-01)
bmy, 16 May 2003 - Added function &getCmpFile 
bmy, 05 Jun 2003 - Now use Bourne Shell to return home dir of the user
                 - updated comments
bmy, 19 Mar 2004 - Added &getMakeFile and &getModelType routines
                 - Now append model type to $runDir
bmy, 01 Feb 2005 - Now make sure DAY OF YEAR is 3 digits in &dateString()
bmy, 15 Feb 2005 - Now put log file in $rundir/logs directory
                 - Now put cmp & job files in $rundir/jobs directory

=head1 AUTHOR

Bob Yantosca (bmy@io.harvard.edu)

=head1 SEE ALSO

bmrun
bmprint
bmtar

=head1 COPYRIGHT

Copyright 2002-2005, Bob Yantosca.  All rights reserved.

=cut

#------------------------------------------------------------------------------

package BenchMark_1yr;

require 5.003;      # need this version of Perl or newer
use English;        # Use English language
use Carp;           # Get detailed error messages
use strict;         # Force explicit variable declarations (like IMPLICIT NONE)

BEGIN {

  #=========================================================================
  # The BEGIN method lists the names to export to the calling routine
  #=========================================================================
  use Exporter ();
  use vars     qw( $VERSION @ISA @EXPORT_OK );

  # Version number
  $VERSION   = 1.00;                                       

  # Export method
  @ISA       = qw( Exporter );                             

  # Export on request
  @EXPORT_OK = qw( &printSeparator  &getUser       &getVersionString
                   &getCodeDir      &getInputDir   &getRootDir  
                   &getRunDir       &getOutputDir  &getJobFile  
                   &getLogFile      &getCmpFile    &getMakeFile
                   &getModelType    &dateString );  
}

#------------------------------------------------------------------------------

sub printSeparator() {

  #===========================================================================
  # Subroutine printSeparator prints a separator line to stdout (bmy, 8/9/02)
  #===========================================================================

  # Define separator line
  my $line = '========================================' .
             '=======================================';

  # Print the separator line plus a newline
  print $line;
  print "\n";

  # Exit normally
  return( 0 );
}

#------------------------------------------------------------------------------

sub getUser() {

  #==========================================================================
  # Subroutine getUser returns the user's home directory path.
  # (bmy, 8/9/02, 6/5/03)
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $user = &getUser();
  #
  # NOTES:
  # (1 ) Now use Bourne Shell to return user's home directory (bmy, 6/5/03)
  #==========================================================================

  # Define user's path (change as necessary)
  my $user = "";

  # Use Bourne Shell to return user's home directory path
  $user = qx( /bin/sh "echo \$HOME" );

  # Remove the newline character
  chomp( $user );

  # Return to calling program
  return( $user );  
}

#------------------------------------------------------------------------------

sub getVersionString($) {

  #==========================================================================
  # Subroutine getVersionString returns the version number as a string.
  # (bmy, 8/9/02, 3/23/03)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number (e.g. 5.01)
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $vString : Version string (e.g. v5-01)
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $vString = &getVersionString( $vNum );
  #
  # NOTES:
  # (1 ) Now can handle branch versions (e.g. v5-03-01) (bmy, 3/23/03)
  #==========================================================================

  # Arguments
  my ( $vNum ) = @_;

  # Local variables
  my $vString = "";

  #==========================================================================
  # getVersionString begins here!
  #==========================================================================
  
  # Replace . with - in the version string
  $vString = "v$vNum";
  $vString =~ s/\./\-/g;

  # Return to calling program
  return( $vString );
}

#------------------------------------------------------------------------------

sub getCodeDir($) {

  #==========================================================================
  # Subroutine getCodeDir returns the directory in which the GEOS-CHEM
  # source code is located. (bmy, 8/9/02)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $codeDir : GEOS-CHEM source code directory
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $codeDir = &getCodeDir( $vNum );
  #==========================================================================

  # Arguments
  my ( $vNum ) = @_;

  # Local variables
  my $user    = "";
  my $vString = "";
  my $codeDir = "";

  #==========================================================================
  # getCodeDir begins here!
  #==========================================================================

  # Get version string
  $vString = &getVersionString( $vNum );

  # Directory where the GEOS-CHEM code resides
  $user    = &getUser();
  $codeDir = "$user/Code.$vString";

  # If $codeDir is not valid, exit w/ err msg
  if ( ! -d $codeDir ) {
    &printSeparator();
    print "run.std: $codeDir is not a valid directory!\n";
    print "run.std: STOP in routine getCodeDir!\n";
    &printSeparator();
    exit(1)
  }

  # Return to calling program
  return( $codeDir );
}

#------------------------------------------------------------------------------

sub getInputDir($$) {

  #==========================================================================
  # Subroutine getInputDir returns the directory where the input files for
  # the benchmark run are located. (bmy, 8/9/02, 3/19/04)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum     : Version number
  # (1 ) $simType  : Simulation type (Radon or full chemistry)    
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $inputDir : Directory containing Radon or full chem input files
  #
  # Calling Sequence
  # -------------------------------------------------------------------------
  # $inputDir = &getInputDir( $simType );
  #
  # NOTES:
  # (1 ) Now append the model type to $inputDir (bmy, 3/19/04)
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_;

  # Local variables
  my $codeDir   = "";
  my $inputDir  = "";
  my $modelType = "";
  my $user      = "";

  #==========================================================================
  # getInputDir begins here!  
  #==========================================================================

  # Get code directory
  $codeDir   = &getCodeDir( $vNum );

  # Get model type
  $modelType = &getModelType( $codeDir );

  # Get the input directory
  $user     = &getUser();
  $inputDir = "$user/stdrun_1yr/input/$simType.$modelType";

  # Return to calling program
  return( $inputDir );
}

#------------------------------------------------------------------------------

sub getRootDir($) {

  #==========================================================================
  # Subroutine getRootDir returns the top-level directory for GEOS-CHEM
  # benchmark runs. (bmy, 8/9/02)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $rootDir : Top-level directory for GEOS-CHEM runs
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $rootDir = &getRootDir( $vNum );
  #
  # NOTES:
  #==========================================================================

  # Arguments
  my ( $vNum ) = @_;

  # Local variables
  my $user    = "";
  my $vString = "";
  my $rootDir = "";

  #==========================================================================
  # getRootDir begins here!
  #==========================================================================

  # Get version string
  $vString = &getVersionString( $vNum );

  # Parent directory for all benchmark runs
  $user    = &getUser();
  $rootDir = "$user/stdrun_1yr/runs/geos.$vString";

  # If the root directory doesn't exist, make it
  if ( ! -d $rootDir ) { mkdir( $rootDir, 0755 ); }

  # Return to calling program
  return( $rootDir );
}

#------------------------------------------------------------------------------

sub getRunDir($$) {

  #==========================================================================
  # Subroutine getRunDir returns the name of the benchmark run directory 
  # for a particular GEOS-CHEM version and simulation type. (bmy, 3/19/04)
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number (e.g. 5.01)
  # (2 ) $simType : Simulation type (e.g. "Rn" or "full")   
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $runDir  : Benchmark run directory name
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # &runDir = &getRunDir( $vNum, $simType );
  #
  # NOTES:
  # (1 ) Now append model type to the run directory (bmy, 3/19/04)
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_;

  # Local variables
  my $codeDir   = "";
  my $modelType = "";
  my $vString   = "";
  my $rootDir   = "";
  my $runDir    = "";

  #==========================================================================
  # getRunDir begins here!
  #==========================================================================
  
  # Get code directory
  $codeDir   = &getCodeDir( $vNum );

  # Get model type
  $modelType = &getModelType( $codeDir );

  # Get root directory $rootDir
  $rootDir   = &getRootDir( $vNum, $simType );

  # The run directory will be a subdirectory of $rootDir
  $runDir    = "$rootDir/run.$simType.$modelType";
 
  # Return to calling program
  return( $runDir );
}

#------------------------------------------------------------------------------

sub getOutputDir($$) {

  #==========================================================================
  # Subroutine getOutputDir returns the name of the benchmark run directory 
  # for a particular GEOS-CHEM version and simulation type. (bmy, 8/9/02)
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number (e.g. 5.01)
  # (2 ) $simType : Simulation type (e.g. "Rn" or "full")   
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $outpuDir  : Benchmark run directory name
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # &runDir = &getRunDir( $vNum, $simType );
  #
  # NOTES:
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_;

  # Local variables
  my $runDir    = "";
  my $outputDir = "";

  #==========================================================================
  # getOutputDir begins here!
  #==========================================================================

  # Get the run directory
  $runDir = &getRunDir( $vNum, $simType );
 
  # Output directory is a subdirectory of $runDir
  $outputDir = "$runDir/output";

  # If $runDir does not exist, create it
  if ( ! -d $outputDir ) { mkdir( $outputDir, 0755 ); }

  # Return to calling program
  return( $outputDir );
}

#------------------------------------------------------------------------------
 
sub getJobFile($$) {

  #==========================================================================
  # Subroutine getJobFile returns the name of the Perl script which will
  # compile and run the GEOS-CHEM model. (bmy, 8/9/02, 2/15/05)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  # (2 ) $simType : Simulation type (Radon or full chemistry)  
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $jobFile : Name of Perl script which compiles & runs the model
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $jobFile = &getJobFile( $vNum, $simType );
  #
  # NOTES:
  # (1 ) Now place job file in "jobs/" subdirectory (bmy, 2/15/05)
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_; 

  # Local variables
  my $vString = "";  
  my $runDir  = "";
  my $jobFile = "";

  #==========================================================================
  # getJobFile begins here!
  #==========================================================================

  # Get the version string
  $vString = &getVersionString( $vNum );

  # Get the run directory
  $runDir = &getRunDir( $vNum, $simType );

  # Place the job file in the run directory
  $jobFile = "$runDir/jobs/$vString.run";

  # Return to calling program
  return( $jobFile );
}

#------------------------------------------------------------------------------

sub getCmpFile($$) {

  #==========================================================================
  # Subroutine getCmpFile returns the name of the compilation script for
  # the GEOS-CHEM benchmark run. (bmy, 5/16/03, 2/15/05)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  # (2 ) $simType : Simulation type (Radon or full chemistry)  
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $cmpFile : Name of Perl script which compiles & runs the model
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $cmpFile = &getCmpFile( $vNum, $simType );
  #
  # NOTES:
  # (1 ) Now place cmp file in "jobs/" subdirectory (bmy, 2/15/05)
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_; 

  # Local variables
  my $vString = "";
  my $runDir  = "";
  my $cmpFile = "";

  #==========================================================================
  # getCmpFile begins here!
  #==========================================================================

  # Get the version string
  $vString = &getVersionString( $vNum );

  # Get the run directory
  $runDir = &getRunDir( $vNum, $simType );

  # Place the log file in the run directory
  $cmpFile = "$runDir/jobs/$vString.cmp";
    
  # Return to calling program
  return( $cmpFile );
}

#------------------------------------------------------------------------------

sub getLogFile($$) {

  #==========================================================================
  # Subroutine getCmpFile returns the name of the log file for the 
  # GEOS-CHEM benchmark run. (bmy, 8/20/02, 2/15/05)
  #
  # Arguments as Input:
  # -------------------------------------------------------------------------
  # (1 ) $vNum    : Version number
  # (2 ) $simType : Simulation type (Radon or full chemistry)  
  #
  # Arguments as Output:
  # -------------------------------------------------------------------------
  # (1 ) $cmpFile : Name of Perl script which compiles & runs the model
  #
  # Calling Sequence:
  # -------------------------------------------------------------------------
  # $cmpFile = &getCmpFile( $vNum, $simType );
  #
  # NOTES:
  # (1 ) Now place log file in "logs/" subdirectory (bmy, 2/15/05)
  #==========================================================================

  # Arguments
  my ( $vNum, $simType ) = @_; 

  # Local variables
  my $vString = "";
  my $runDir  = "";
  my $logFile = "";

  #==========================================================================
  # getLogFile begins here!
  #==========================================================================

  # Get the version string
  $vString = &getVersionString( $vNum );

  # Get the run directory
  $runDir = &getRunDir( $vNum, $simType );

  # Place the log file in the run directory
  $logFile = "$runDir/logs/$vString.log";
    
  # Return to calling program
  return( $logFile );
}

#------------------------------------------------------------------------------

sub getMakeFile($) {

  #=========================================================================
  # Subroutine getMakeFile examines the "define.h" file in order to 
  # determine the name of the makefile which will be used to compile
  # GEOS-CHEM for the given platform and compiler.  (bmy, 12/8/03)
  #
  # Arguments as Input:
  # ------------------------------------------------------------------------  
  # (1 ) $codeDir : Name of the directory where the source code resides.
  #
  # Calling Sequence:
  # ------------------------------------------------------------------------  
  # $makeFile = getMakeFile( $codeDir );
  #
  # NOTES:
  #=========================================================================

  # Arguments
  my ( $codeDir )= @_;

  # Local variables
  my $line  = "";
  my @lines = "";

  # Read "define.h" into a string array
  open( INPUT, "$codeDir/define.h" ) or croak "trun: Can't open define.h";
  chomp( @lines = <INPUT> );
  close( INPUT );

  # Process each line individually
  foreach $line ( @lines ) {

    # Determine makefile name from the defined C-preprocessor switch
    if ( $line =~ "#define" and !( $line =~ "!#define" ) ) { 
      if    ( $line =~ "COMPAQ"    ) { return( "Makefile.compaq" ); }
      elsif ( $line =~ "IBM"       ) { return( "Makefile.ibm"    ); }
      elsif ( $line =~ "LINUX_PGI" ) { return( "Makefile.pgi"    ); } 
      elsif ( $line =~ "LINUX_IFC" ) { return( "Makefile.ifc"    ); } 
      elsif ( $line =~ "LINUX_EFC" ) { return( "Makefile.efc"    ); } 
      elsif ( $line =~ "LINUX"     ) { return( "Makefile.linux"  ); } 
      elsif ( $line =~ 'SGI'       ) { return( "Makefile.sgi"    ); }
      elsif ( $line =~ 'SPARC'     ) { return( "Makefile.sparc"  ); }
    }
  }

  # Otherwise return failure
  return( '' );
}

#------------------------------------------------------------------------------

sub getModelType($) {

  #=========================================================================
  # Subroutine getModelType examines the "define.h" file in order to 
  # determine the model type (GEOS-3 or GEOS-4) (bmy, 3/19/04)
  #
  # Arguments as Input:
  # ------------------------------------------------------------------------  
  # (1 ) $codeDir : Name of the directory where the source code resides
  #
  # Calling Sequence:
  # ------------------------------------------------------------------------  
  # $modelType = getModelType( $codeDir );
  #
  # NOTES:
  #=========================================================================

  # Arguments
  my ( $codeDir )= @_;

  # Local variables
  my $line  = "";
  my @lines = "";

  # Read "define.h" into a string array
  open( INPUT, "$codeDir/define.h" ) or croak "trun: Can't open define.h";
  chomp( @lines = <INPUT> );
  close( INPUT );

  # Process each line individually
  foreach $line ( @lines ) {

    # Determine makefile name from the defined C-preprocessor switch
    if ( $line =~ "#define" and !( $line =~ "!#define" ) ) { 
      if    ( $line =~ "GEOS_1"     ) { return( "geos1" ); }
      elsif ( $line =~ "GEOS_STRAT" ) { return( "geoss" ); }
      elsif ( $line =~ "GEOS_3"     ) { return( "geos3" ); } 
      elsif ( $line =~ "GEOS_4"     ) { return( "geos4" ); } 
      elsif ( $line =~ "GEOS_5"     ) { return( "geos5" ); } 
    }
  }

  # Otherwise return failure
  return( '' );
}

#------------------------------------------------------------------------------

sub dateString() {

  #=========================================================================
  # Subroutine dateString returns a string for the current date in
  # the format DDDHHMMSS (day of year, hour, min, sec). 
  # (bmy, 9/19/03, 2/1/05)
  #  
  # Calling Sequence:
  # ------------------------------------------------------------------------
  # $dstr = &dateString();
  #
  # NOTES:
  # (1 ) Moved to BenchMark_1yr.pm (bmy, 12/8/03)
  # (2 ) Make sure DAY OF YEAR is always 3 digits (bmy, 2/1/05)
  #========================================================================= 

  # Local variables
  my @a    = localtime();
  my $day  = $a[7] + 1;
  my $hour = $a[2];
  my $min  = $a[1];
  my $sec  = $a[0];

  # Pad with zeroes if necessary
  if ( $day  < 100 ) { $day  = "0$day";  }
  if ( $hour < 10  ) { $hour = "0$hour"; }
  if ( $min  < 10  ) { $min  = "0$min";  } 
  if ( $sec  < 10  ) { $sec  = "0$sec";  }

  # Return to calling program
  return( "$day$hour$min$sec" );
}
