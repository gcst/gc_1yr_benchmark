#!/usr/bin/perl -w

#------------------------------------------------------------------------------
#          Harvard University Atmospheric Chemistry Modeling Group            !
#------------------------------------------------------------------------------
#BOP
#
# !MODULE: mean_oh.pl
#
# !DESCRIPTION: This Perl script computes the mean OH concentration of a 1-yr
#  GEOS-Chem benchmark simulation.  It reads the mean OH values printed to
#  the log file of each successive run stage and computes the average.
#\\
#\\
# !USES:
#
   require 5.003;       
   use strict;
#
# !CALLING SEQUENCE:
#  mean_oh.pl
#
# !REVISION HISTORY: 
#  07 Jun 2011 - R. Yantosca - Initial version
#EOP
#------------------------------------------------------------------------------
#BOC

# Arrays
my @files  = qw( log.20160101 log.20160201 log.20160301 log.20160401 log.20160501 log.20160601 log.20160701 log.20160801 log.20160901 log.20161001 log.20161101 log.20161201 );
my @result = ();

# Scalars
my $file   = "";
my $oh     = 0;

# Loop over all files
foreach $file ( @files ) {

  # Split the line into substrings.  The mean OH value 
  # is in the 4th substring (indexed by $result[3]).
  @result = split( ' ', qx( grep 1e5 $file ) );

  # Sum the 
  $oh    += $result[3];

  # Echo info
  print "Mean OH from $file: $result[3] [1e5 molec/cm3]\n";
}

# Take the average
$oh /= scalar( @files );

# Echo result
print "\nTotal Mean OH: $oh [1e5 molec/cm3]\n";

# Exit normally
exit(0);
#EOC
