pro make_nc

   ; Converts bpch to netCDF for benchmark plots

   ; Diagnostics
   D = [ 'CHEM-L=$', 'IJ-AVG-$', 'PEDGE-$',  'BXHGHT-$', 'OD-MAP-$', $
         'ANTHSRCE', 'BIOFSRCE', 'BIOBSRCE', 'BIOGSRCE',             $
         'NO-AN-$',  'NO-AC-$',  'NO-BIOF',  'NO-BIOB',  'NO-FERT',  $
         'NO-SOIL',  'NO-LI-$',  'CO--SRCE', 'ACETSRCE', 'DMS-BIOG', $
         'SO2-AN-$', 'SO2-AC-$', 'SO2-BIOF', 'SO2-BIOB', 'SO2-NV-$', $
         'SO2-EV-$', 'SO2-SHIP', 'SO4-AN-$', 'SO4-BIOF', 'NH3-ANTH', $
         'NH3-BIOF', 'NH3-BIOB', 'NH3-NATU', 'BC-ANTH',  'BC-BIOF',  $
         'BC-BIOB',  'OC-ANTH',  'OC-BIOF',  'OC-BIOB',  'OC-BIOG',  $
         'DUSTSRCE', 'SALTSRCE', 'PL-BC=$',  'PL-OC=$',  'JV-MAP-$', $
         'TR-PAUSE', 'DXYP',     'UP-FLX-$'                          ]

   ; Output file path
   OutFileName = '{VERSION}.%DATE%.nc'

   ; Make netCDF fles
   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160101', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160201', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160301', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160401', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160501', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160601', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160701', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160801', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20160901', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20161001', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20161101', $
            OutFileName, diagn=D

   ctm_cleanup
   bpch2coards, '../bpch/trac_avg.{VERSION}.20161201', $
            OutFileName, diagn=D

end	
