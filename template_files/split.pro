pro split

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
; NOTE: This routine is not currently required because we split the 1-year
; benchmark into 1-month runs. (mps, 7/27/15)
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

;   ; Splits into monthly bpch files (change as necesary)
;
;   ; Output directory
;   Dir = './'
;
;   InFile = 'trac_avg.{VERSION}.201301010000' 
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130101', $
;      tau=nymd2tau( 20130101 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130201', $
;      tau=nymd2tau( 20130201 )
;   ctm_cleanup
;
;   InFile = 'trac_avg.{VERSION}.201303010000' 
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130301', $
;      tau=nymd2tau( 20130301 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130401', $
;      tau=nymd2tau( 20130401 )
;   ctm_cleanup
;
;   InFile = 'trac_avg.{VERSION}.201305010000' 
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130501', $
;      tau=nymd2tau( 20130501 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130601', $
;      tau=nymd2tau( 20130601 )
;   ctm_cleanup
;
;   InFile = 'trac_avg.{VERSION}.201307010000' 
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130701', $
;      tau=nymd2tau( 20130701 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130801', $
;      tau=nymd2tau( 20120801 )
;   ctm_cleanup
;
;   InFile = 'trac_avg.{VERSION}.201309010000'
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20130901', $
;      tau=nymd2tau( 20120901 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20131001', $
;      tau=nymd2tau( 20121001 )
;   ctm_cleanup
;
;   InFile = 'trac_avg.{VERSION}.201311010000' 
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20131101', $
;      tau=nymd2tau( 20121101 )
;   ctm_cleanup
;   bpch_sep, InFile, Dir+'trac_avg.{VERSION}.20131201', $
;      tau=nymd2tau( 20121201 )
;   ctm_cleanup

end
